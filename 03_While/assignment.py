# ========================
# Задание 1: подсчет гласных.
# УСЛОВИЕ: Подсчет гласных букв в строке.
# Примечание:
# - для простоты на вход принимаем строку из букв латинского алфавита;
# - набор гласных принимаем за 'a', 'e', 'i', 'o', 'u';
# - программа должна быть нечувствительна к регистру.
# Пример: assert vowels("hApPyHalLOweEn!") == 5

def vowels(text):
    vowels_list = ['a', 'e', 'i', 'o', 'u']
    result = 0
    i = 0
    while i < len(text):
        if text[i].lower() in vowels_list:
            result += 1
        i += 1
    return result

assert vowels("hApPyHalLOweEn!") == 5


# ==================================================================================
#
# Задание 2: подсчет вхождений подстроки.
# УСЛОВИЕ: Реализовать код, который будет подсчитывать количество вхождений подстроки "wow" в строке.
# ВХОД: строка
# ВЫХОД: число вхождений подстроки "wow" Пример: assert wower("wowhatamanwowowpalehche") == 3

def wower(text, subtext):
    text = text.lower()
    result = 0
    i = True
    while i:
        if subtext in text:
            result += 1
            text = text.replace(subtext[:len(subtext)], "", 1)
        else:
            i = False
    return result

assert wower("wowhatatamanwowowptataalehche", "ta") == 4


# ================================================================================
#
# Задание 3: упорядоченная подстрока.
# УСЛОВИЕ: Построить код, который будет находить в строке подстроку максимальной длины, в которой буквы упорядочены в алфавитном порядке.
# ВХОД: строка
# ВЫХОД: подстрока
# Пример: assert alphabetical("sabrrtuwacaddabra") == "abrrtuw"

def alphabetical(text):
    text = text.lower()
    previous = ""
    substring = ""
    result = ""
    i = 0
    while i < len(text):
        if text[i] >= previous:
            substring += text[i]
            if len(result) < len(substring):
                result = substring
        else:
            substring = text[i]
        previous = text[i]
        i += 1

    return result

assert alphabetical("sabrrtuwacaddabra") == "abrrtuw"


# ================================================================================
#
# Задание 4: уникальный набор.
# УСЛОВИЕ: Реализовать код, который принимает список елементов и убирает из него все дубликаты (формирует список уникальных элементов).
# Сделать вариант с сохранением порядка следования элементов и вариант, в кот. сортировка элементов не принципиальна.
# Пример:
def unique_ordered(in_list):
    unique_list = set(in_list)

    dictionary = {}

    for i in unique_list:
        if isinstance(i, tuple):
            dictionary[str(i[0])] = i
        else:
            dictionary[str(i)] = i

    sorted_keys = sorted(dictionary.keys(), reverse=True)

    final = []
    for i in sorted_keys:
        final.append(dictionary[i])

    return final

assert unique_ordered(["a", 5, 2, 5, (1, "a"), "a"]) == ["a", 5, 2, (1, "a")]



def unique_ordered_while(in_list):
    result = []
    i = 0
    while i < len(in_list):
        if in_list[i] not in result:
            result.append(in_list[i])
        i += 1

    return result

assert unique_ordered_while(["a", 5, 2, 5, (1, "a"), "a"]) == ["a", 5, 2, (1, "a")]



def unique_disordered(list):
    return set(list)

# print(unique_disordered(["a", 5, 2, 5, (1, "a"), "a"]))
# assert unique_disordered(["a", 5, 2, 5, (1, "a"), "a"]) == [2, "a", 5, (1, "a")]


# ================================================================================
# Задание 5: каждый третий.
# УСЛОВИЕ: Реализовать код который принимает кортеж и возвращает прореженный кортеж, оставляя только каждый третий элемент. Реализовать задачу двумя разными вариантами.
# Пример:
def thirds(input_tuple):
    result = []
    i = 2
    while i < len(input_tuple):
        result.append(input_tuple[i])
        i += 3
    result = tuple(result)
    return result

assert thirds((1,2,3,4,5,6,7,8,9,0,'a','b','c')) == (3,6,9,'b')


def thirds2(input_tuple):
    input_list = list(input_tuple)
    result = []

    i = 0
    pop_index = 0
    while i < len(input_tuple):
        if (i + 1) % 3 == 0:
            pop_index += 1
        else:
            input_list.pop(pop_index)
        i += 1

    result = tuple(input_list)
    return result

assert thirds2((1,2,3,4,5,6,7,8,9,0,'a','b','c')) == (3,6,9,'b')


def thirds3(input_tuple):
    return input_tuple[2::3]

assert thirds3((1,2,3,4,5,6,7,8,9,0,'a','b','c')) == (3,6,9,'b')


# ================================================================================
#
# Задание 6: XYZ. (OPTIONAL)
# УСЛОВИЕ: Написать функцию без использования каких-либо условных выражений,
# а применив встроенные функции min() и max(),
# которая принимает три аргумента - числа X, Y, Z
# и возвращает один из вариантов в порядке возрастания значимости:
# - X, если Y < X;
# - Z, если Y > Z;
# - Y, при ином раскладе.

# Без использования условных выражений
def xyz(x, y, z):
    return {
    x > y: x,
    x < y > z: z,
    x < y < z: y
}[True]

print(xyz(4, 3, 2))
print(xyz(4, 1, 2))
print(xyz(1, 5, 3))
print(xyz(1, 2, 3))


# С использованием min() и max()

def xyz2(x, y, z):
    if min(x, y) == y:
        return x
    elif max(y, z) == y:
        return z
    else:
        return y

print(xyz2(4, 3, 2))
print(xyz2(4, 1, 2))
print(xyz2(1, 5, 3))
print(xyz2(1, 2, 3))
