a = [0, 1, 2, -3, 1, 5]

# Min element
x = a[0]

for n in a:
    if n < x:
        x = n

print(x)       


# Sum of the elements
result = 0

for n in a:
    result += n

print(result)


# Sum of lements
a = [[0, 1, 2, -3, 5, 1], 
    [0, 0, 0, 1, 1],
    [0, 0, 1, 1, 1],
    [0, 1, 1, 1, 1]]

result2 = 0

for list_element in a:
    for n in list_element:
        result2 += n

print(result2)