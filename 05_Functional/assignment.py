#1 Симметрия.
# УСЛОВИЕ: Фрагмент кода, который принимает строку и определяет симметрична ли строка. (Возвращает True или False).
def symmetry(text):
    i = 0
    result = True
    while i <= len(text) / 2:
        if text[i] != text[len(text) - 1 - i]:
            result = False
        i += 1
    return result

assert symmetry("abba") == True
assert symmetry("abrbrba") == True
assert symmetry("abrbrrba") == False


# FIX
# 1 line solution
def symmetry2(text):
    return True if text == text[::-1] else False

assert symmetry2("abba") == True
assert symmetry2("abrbrba") == True
assert symmetry2("abrbrrba") == False


#2 Триделение.
# УСЛОВИЕ: Фрагмент кода, который принимает список любых чисел и возвращает словарь вида:
# {число: boolean}, где: boolean - True или False в зависимости делится ли число на 3 без остатка.
# Решить задачу двумя способами - с помощю dict. comprehension и без.
# с помощю dict
def triples_1(numbers):
    return {k: k % 3 == 0 for k in numbers}

assert triples_1([3, 7, 12]) == {3: True, 12: True, 7: False}
assert triples_1([9, 1, 2, 5, 9, 33]) == {1: False, 2: False, 5: False, 9: True, 33: True}

# без
def triples_2(numbers):
    result = {}
    for n in numbers:
        result[n] = n % 3 == 0
    return result

assert triples_2([3, 7, 12]) == {3: True, 12: True, 7: False}
assert triples_2([9, 1, 2, 5, 9, 33]) == {1: False, 2: False, 5: False, 9: True, 33: True}


#3 Чет-нечет.
# УСЛОВИЕ: Фрагмент кода, который принимает список любых чисел и
# фильтрует его по четным (удаляет нечетные), если количество элементов в списке является четным
# и наоборот (удаляет четные, если элементов изначально нечетное количество).
# Решить задачу с помошью list comprehension.
def evenodd(numbers):
    return [x for x in numbers if x % 2 == 0] if len(numbers) % 2 == 0 \
        else [x for x in numbers if x % 2 != 0]

assert evenodd([3, 7, 12]) == [3,7]
assert evenodd([3, 7, 12, 7]) == [12]



#4 И ещё, как бонус,
# перепишите с помощью выражения генератора решение задачи на нахождение суммы чисел двумерного массива,
# что мы на уроке решали. Такой вариант гарантированно оценит ваш гипотетический собеседователь.
# Sum of lements
a = [[0, 1, 2, -3, 5, 1],
    [0, 0, 0, 1, 1],
    [0, 0, 1, 1, 1],
    [0, 1, 1, 1, 1]]

# FIX
def sum2(numbers):
    result = sum([n for row in numbers for n in row])
    # Ruslan way
    # summ2 = sum(a[x][y] for x in range(len(a)) for y in range(len(a[x])))
    return result

assert sum2(a) == 15