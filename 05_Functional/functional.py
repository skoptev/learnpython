# Functional programming: without saving state

# Lists, sets, dictionaries generators
# list comprehension
numbs = [1, 2, 3, 4, 5]

result = [x * x for x in numbs if x > 3]
         [stmt for var in iterable if predicate]

[x * x for x in [1, 2, 3, 4, 5] if x % 2 == 0]                  [4, 16]
[x + y for x, y in ((1, 2), (2, 3), (3, 4))]                    [3, 5, 7]
[x.upper() for x in ['foo', 'bar', 'test'] if len(x) < 4]       ['FOO', 'BAR']

users = [{'name': 'ivan', 'age': 29},
         {'name': 'juan', 'age': 31}]
[user['name'] for user in users if user['age'] > 30]            ['juan']

some_dict = {'foo': 'test', 'bar': None}
[k for (k,v) in some_dict.items() if v is not None]             ['foo']


# set comprehension
{x for x in [1, 2, 2, 2, 3, 1]}                                 {1, 2, 3}


# dictionary comprehension
{k: v for (k, v) in [('foo', 1), ('bar', 2)]}                   {'bar': 2, 'foo':1}

# fix string keys
mydict = {'1': 'value1', '2': 'value2'}
{int(k): v for (k, v) in mydict.items()}                        {1: 'value1', 2: 'value2'}

#reverse dictionary
{v: k for (k, v) in mydict}                                     {'value1': '1', 'value2': '2'}


# generator expression (object generator)
# lazy sequence
# cannot access value by index
# length is unknown
# cannot iterate backwards
gen = (x for x in [1, 2, 3])


# itertools module
import itertools
print(''.join(itertools.chain('ABC','DEF'))                     ABCDEF
combinations('ABCD', 2)                                         AB AC AD BC BD CD