# ну и домашнее задание:

# 1. Реверс строки. Условие:
# Преобразовать строку "ecnalubma" в её зеркальное отражение(реверсировать). Сделать это двумя способами.
# input: 'ecnalubma'
# output: 'ambulance'
print("Assignment 1")
import stringrevert

print("----------------------")


# 2. Реверс строки 2.
# В строке изменить последовательность слов на обратную:
# input: 'one two three four'
# output: 'four three two one'
print("Assignment 2")
from splitreverse import split_and_reverse

split_and_reverse("one two three four")

print("----------------------")

# 3. Напишите программу, которая вычисляет сумму цифр случайного трёхзначного числа.
print("Assignment 3")
import sum