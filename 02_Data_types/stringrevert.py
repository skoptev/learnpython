# 1. Реверс строки. Условие:
# Преобразовать строку "ecnalubma" в её зеркальное отражение(реверсировать). Сделать это двумя способами.
# input: 'ecnalubma'
# output: 'ambulance'

# 1st way
original_string = input("Provide a string: ")
result_string = original_string[::-1]
print(result_string)


# 2nd way
original_string = input("Provide a string: ")
result_string = list(original_string)
result_string.reverse()
result_string = "".join(result_string)
print(result_string)