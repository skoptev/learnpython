# 2. Реверс строки 2.
# В строке изменить последовательность слов на обратную:
# input: 'one two three four'
# output: 'four three two one'

def split_and_reverse(string : str):
    words = string.split(" ")
    words.reverse()

    print(" ".join(words))