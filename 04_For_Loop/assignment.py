# 1. Заполните список случайными числами.
# Используйте цикл for, функции range() and randint()
from random import randint


def randlist(length):
    result = []
    for i in range(length):
        result.append(randint(0, 100))
    return result

print(randlist(5))


# 2. Если объект range (диапазон) передать встроенной в Python функции list(), то она преобразует его к списку.
# Создайте таким образом список с элементами от 0 до 100 с шагом 17.
def rangeList(start, end, step):
    return list(range(start, end, step))

print(rangeList(0, 100, 17))


# 3. В задданом списке, состоящем из положительных и отрицательны чисел, посчитайте количество отрицательных элементов.
# Выведите результат на экран
def negativeCount(in_list):
    result = 0
    for i in in_list:
        if i < 0:
            result += 1
    print(result)

negativeCount([-1, 0, 9, -3, 4, 6])


# 4. Напишите программу, которая заполняет список пятью словами, введенными с клавиатуры,
# измеряет длинну каждого слова и добавляет полученное значение в другой список.
# Оба списка должны выводиться на экран
def countWords(wordsList):
    result = []
    for word in wordsList:
        result.append(len(word))
    print(f"Words {wordsList} has length {result}")
    return result

assert countWords(['yes', 'no', 'maybe', 'ok', 'what']) == [3, 2, 5, 2, 4]


# 5. И ещё одна маленькая задачка:
# даны два списка
# a = [1,2,3,4,5]
# b = ['one','two','three','four','five']
# нужно обьединить их в словарь двумя разными способами
# список b - это ключи
# список а - значения
def listsMerge(list1, list2):
    result1 = {}
    result2 = {}
    i = 0
    while i < len(list1):
        result1[list1[i]] = list2[i]
        result2[list2[i]] = list1[i]
        i += 1
    print(result1)
    print(result2)

listsMerge([1,2,3,4,5], ['one','two','three','four','five'])
