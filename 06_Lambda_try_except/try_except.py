# Программа запрашивает ввод двух значений.
# Если хотя бы одно из них не число, должна выполняться конкатенация.
# В остальных случаях числа суммируются
def sum_or_concat(a, b):
    try:
        result = float(a) + float(b)
    except (TypeError, ValueError):
        result = str(a) + str(b)
        print('Хотя бы одно из введенных значений не число.')
    else:
        print('Суммирование двух чисел прошло успешно.')
    finally:
        print(result)
        return result


a = input('Введите число 1: ')
b = input('Введите число 2: ')

sum_or_concat(a, b)


assert sum_or_concat(1, 1) == 2
assert sum_or_concat(4, 5) == 9.0
assert sum_or_concat('a', 5) == 'a5'