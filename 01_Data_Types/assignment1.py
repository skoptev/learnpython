# Assignment 1.1
var_int = 10
var_float = 8.4
var_str = "No"


# Assignment 1.2
big_int = var_int * 3.5


# Assignment 1.3
var_float /= 1


# Assignment 1.4
var_int / var_float
big_int / var_float


# Assignment 1.5
var_str = var_str * 2 + "Yes" * 3


# Assignment 1.6
print(var_int)
print(var_float)
print(big_int)
print(var_str)