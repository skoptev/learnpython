# для желающих почитать на досуге
# https://habr.com/en/post/132554/

#4 Самое длинное слово.

# УСЛОВИЕ:
# Найти слово максимальной длины в тексте.
# Вывести это слово. Если таких слов несколько - вывести все.
#
# Текст:
import string

def longest_word(text):
    words = text.translate(str.maketrans('', '', string.punctuation)).split(' ')
    longestWord = words[0]
    for word in words:
        if len(word) > len(longestWord):
            longestWord = word
    return longestWord


text = 'Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Proin eget tortor risus. Curabitur11 non nulla sit amet nisl tempus convallis quis ac lectus. Donec rutrum congue leo eget malesuada.'

assert len(longest_word(text)) == 11


#5 Imports.

# УСЛОВИЕ:
# Произвести импортирование модулей стандартной библиотеки Python: "os" и "sys".
# Вывести справку по всем функциям каждого модуля.
# import os, sys
#
# print(dir(os))
# for function in dir(os):
#     print('----------------')
#     help(function)
#
# print(dir(sys))
# for function in dir(sys):
#     print('----------------')
#     help(function)



#6 Частота буквы.

# УСЛОВИЕ:
# Посчитать процентное соотношение букв в тексте. Заглавные и строчные приравниваются.
# Вывести словарь: ключ - буква, значение - процент, в котором эта буква встречается в тексте.
#
# Текст:
# Proin eget tortor risus. Cras ultricies ligula sed magna dictum porta. Donec rutrum congue leo eget malesuada.
#

def symbolsCount(text):
    text = text.lower()
    result = {}
    for a in text:
        if result.keys().__contains__(a):
            result[a] += 1
        else:
            result[a] = 1

    for key in result.keys():
        result[key] *= 100.0 / len(text)

    return result

assert symbolsCount("ABCda") == {'a': 40.0, 'b': 20.0, 'c': 20.0, 'd': 20.0}